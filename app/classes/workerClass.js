//Import Human class
const {Human} = require('./humanClass'); 

const Worker = class extends Human {
  constructor(fullName, DOB, homeTown, job, workPlace, salary) {
    super(fullName, DOB, homeTown);
    this.job = job;
    this.workPlace = workPlace;
    this.salary = salary;
  }
  getInfo() {
    console.log("In ra info của 1 công nhân");
    var info = {
      fullName: this.fullName, 
      DOB: this.DOB, 
      homeTown: this.homeTown, 
      job: this.job, 
      workPlace: this.workPlace, 
      salary: this.salary
    }
    return info;
  }
}

let obj = new Worker("Thang", "20/09/1991", "SG", "coder", "QTSC", 15000000);
console.log(obj.getInfo());